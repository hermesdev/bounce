<?php require_once('include/header.php'); ?>

	<div class="container-fluid background">
		<div class="layout-wrapper">
			<div class="row panel-control">
				<slider
					class="main-slider"
					ng-model="slider.value"
					value="slideEnded()"
					min="testOptions.min"
					step="1"
					max="testOptions.max"
					ticks="[0, 1, 2, 3, 4]">
				</slider>
				<div class="slider-labels">
					<ul class="labels-top">
						<li>events</li>
						<li>local</li>
						<li>global</li>
					</ul>
					<ul class="labels-values">
						<li>100</li>
						<li>1</li>
						<li>10</li>
						<li>100</li>
						<li>∞</li>
					</ul>
					<ul class="labels-units">
						<li>yards</li>
						<li>mile</li>
						<li>miles</li>
						<li>miles</li>
						<li>galactic</li>
					</ul>
				</div>
				<div id="bounce-mode" class="btn-group">
	        		<label ng-click="changeDisplayMode(0)" class="btn btn-success" ng-model="radioModel" btn-radio="'Everyone'" uncheckable>Everyone</label>
	        		<label ng-click="changeDisplayMode(1)" class="btn btn-success" ng-model="radioModel" btn-radio="'Popular'" uncheckable>Popular</label>
	  			</div>
			</div><!-- end row -->

			<form name="createBounceForm" 
						ng-show="userLoggedIn"
						novalidate
						enctype="multipart/form-data">
			<div flow-init="{singleFile:true}"
					 flow-files-submitted="$flow.upload()"
					 flow-file-added="!!validImageFormats[$file.getExtension()]"
					 flow-name="bounce.flow">
				<div class="createBounce" ng-class="{ 'margin-extended':!visibleBounceControls }">
					<textarea
						class="form-control" id="bounceInput"
						ng-attr-placeholder="{{ textareaPlaceholder }}"
						ng-model="bounce.text"
						name="bounceText"
						ng-click="visibleBounceControls=true;fullHeightTextarea=true; globals.refreshWhenScrollTop=800"
						ng-class="{'full-height': fullHeightTextarea, 'has-error': bounceTextOrPictureRequiredError }"
						required
					></textarea>
					<span class="input-group-btn" style="text-align: right">
	        	<input class="bounce-btn" type="image" src="img/pencil.png" ng-click="createBounce()" alt="Submit">
	      	</span>
				</div>
				<div class="createButtons noselect" ng-show="visibleBounceControls">
					<div class="btn-group" role="group" aria-label="...">
						<div class="btn-group" role="group">

						    <span id="photo-btn" class="btn btn-default visible-xs" flow-btn><i class="fa fa-camera"></i></span>
						</div>
						<div class="btn-group" role="group">
							<select id="rad-select" class="form-control radius" ng-model="bounce.radius" style="font-family: 'FontAwesome', Roboto" >
								<option value="-1" disabled>Distance</option>
								<option value="91.44">100 yards</option>
								<option value="1609.34">1 mile</option>
								<option value="16093.4">10 miles</option>
								<option value="160934">100 miles</option>
								<option value="infin">Infinity</option>
							</select>
						</div>
						<div class="btn-group" role="group">
							<select  id="exp-select" class="form-control expiration" ng-model="bounce.expiration" style="font-family: 'FontAwesome', Roboto;">
								<option value="-1" disabled>Expiration</option>
								<option value="10m">10 min</option>
								<option value="1h">1 hr</option>
								<option value="24h">1 day</option>
								<option value="168h">1 wk</option>
								<option value="720h">1 mo</option>
								<option value="eternity">Eternity</option>
							</select>
						</div>
						<div class="btn-group" id="share-icons" role="group">
							<div class="checkbox">
									<input type="checkbox" ng-model="bounce.location" id="location-check">
									<label for="location-check"><i class="fa fa-eye"></i><span class="extra">Share location?</span></label>
							</div>
					  </div>
						<div class="btn-group" id="share-icons" role="group">
							<div class="checkbox">
									<input type="checkbox" id="facebook-check" ng-model="bounce.shareFacebook" ng-change="loginForSharingWithFacebook()">
									<label for="facebook-check"><i class="fa fa-facebook"></i><span class="extra">Post on Facebook?</span></label>
							</div>
						</div>
						<div class="btn-group" id="share-icons" role="group">
							<div class="checkbox">
									<input type="checkbox" id="twitter-check" ng-model="bounce.shareTwitter" ng-change="loginForSharingWithTwitter()">
									<label for="twitter-check"><i class="fa fa-twitter"></i><span class="extra">Tweet on Twitter?</span></label>
							</div>
						</div>
					</div>


				  <div class="drop hidden-xs" flow-drop ng-class="dropClass">
				    <span class="btn btn-default" flow-btn><i class="fa fa-camera"></i> Upload File</span>
				    <strong>OR</strong>
				    Drag And Drop your file here
				  </div>

			    <div ng-repeat="file in $flow.files">
			     	{{file.name}}
					  <div class="progress progress-striped" ng-class="{active: file.isUploading()}">
			        <div class="progress-bar" role="progressbar"
			             aria-valuemin="0"
			             aria-valuemax="100"
			             ng-style="{width: progressPercentage + '%'}">
			          <span class="sr-only">{{progressPercentage.progress()}}% Complete</span>
			        </div>
			      </div>
			    </div><!-- end ng-repeat -->
			    
					<span class="validation-error" ng-show="bounceTextOrPictureRequiredError">Text or image is required</span>
					<span class="validation-error" ng-show="bounceExpirationRadiusRequiredError">Expiration and Radius are required</span>
					<span class="validation-error" ng-show="bouncePictureFormatError">Image can be jpeg or png</span>
					<!-- <span class="validation-error" ng-show="bouncePictureStillUploadingError">Chill! Image still uploading - try again in a few seconds</span> -->
					<span class="validation-error" ng-show="bouncePictureTooLargeError">Maximum photo file size is 2mb.  Please upload a smaller photo.</span>
					
				</div><!-- end createButtons -->

			</div>
			</form><!-- end createBounceForm -->

			<!-- display the bounces -->
			<div class="bounce" ng-scroll-repeat="bounce in bounces" page-size="10" tolerance="50">
				<bounce-structure bounce="bounce"></bounce-structure>
			</div>

		</div><!-- end layout-wrapper -->
	</div><!-- end container-fluid -->

</div><!-- end ng-controller CreateBounceController -->

<?php require_once('include/footer.php'); ?>