
/**
 * toFixed mimic the pure js toFixed function as a filter
 *
 * @param int digit the decimal precision
 */
angular.module('bouncefilters').filter('toFixed', function() {
  return function(input, precision) {
	return input.toFixed(precision);
  };
});