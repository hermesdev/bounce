/**
 * getDurationStr get the duration between two dates, formatted with the good
 * unit, from year to minute
 *
 * @var Date date1 the first date
 * @var Date date2 the seconde date
 */
function getPrettyDurationStr(date1, date2) {
	var duration = getDurationObject(date1, date2);

	if(duration.y >= 1) {
		return duration.y !== 1 ? duration.y + ' years' : duration.y + ' year';
	} else if(duration.m >= 1) {
		return duration.m !== 1 ? duration.m + ' months' : duration.m + ' month';
	} else if(duration.w >= 1) {
		return duration.w !== 1 ? duration.w + ' weeks' : duration.w + ' week';
	} else if(duration.d >= 1) {
		return duration.d !== 1 ? duration.d + ' days' : duration.d + ' day';
	} else if(duration.h >= 1) {
		return duration.h !== 1 ? duration.h + ' hours' : duration.h + ' hour';
	} else if(duration.min >= 1) {
		return duration.min !== 1 ? duration.min + ' minutes' : duration.min + ' minute';
	} else if(duration.s >= 1) {
		return duration.s !== 1 ? duration.s + ' seconds' : duration.s + ' second';
	}
}

function getDurationObject(date1, date2) {
	var ONE_SECOND = 1000,
	    ONE_MINUTE = ONE_SECOND * 60,
			ONE_HOUR   = ONE_MINUTE * 60,
			ONE_DAY    = ONE_HOUR * 24,
			ONE_WEEK   = ONE_DAY * 7,
			ONE_MONTH  = ONE_WEEK * 4.348,
	    ONE_YEAR   = ONE_MONTH * 12;

	var duration_ms = Math.abs(date1.getTime() - date2.getTime());

	return {
		y: Math.floor(duration_ms / ONE_YEAR),
		m: Math.floor(duration_ms / ONE_MONTH),
		w: Math.floor(duration_ms / ONE_WEEK),
		d: Math.floor(duration_ms / ONE_DAY),
		h: Math.floor(duration_ms / ONE_HOUR),
		min: Math.floor(duration_ms / ONE_MINUTE),
		s: Math.floor(duration_ms / ONE_SECOND)
	};
}
