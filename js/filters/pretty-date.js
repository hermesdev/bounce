
/**
 *	bounceEpiration filter will convert the post date into a duration
 * 	that tells how long ago the bounce has been posted
 *	
 */
angular.module('bouncefilters').filter('prettyDurationStr', function() {
  return function(input) {

  	// replace the space inbetween the date and the time 
  	// to create a new Date object
	var date = new Date(input.replace(' ', 'T') + 'Z');

	return getPrettyDurationStr(new Date(), date);
  };
});