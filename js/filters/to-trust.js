
/**
 * toTrust trust html strings
 *
 * @param int digit the decimal precision
 */
angular.module('bouncefilters').filter('toTrust', ['$sce', function($sce) {
	return function(text) {
    	return $sce.trustAsHtml(text);
    };
}]);