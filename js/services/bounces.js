

angular.module('BounceApp').factory('bounces', ['$http', 'Globals', function($http, Globals) {

	var Bounce = function() {
    // this.newItems = [];
    this.after = '';
  };

  Bounce.prototype.nextPage = function(mode, urlParams) {

    // set the infinity to 1 if the slider is on infinity
    if(urlParams.distance === -1) {
      var infinity = 1;
    } else {
      var infinity = 0;
    }

    // create the url to query the Bounce API
    var baseUrl = Globals.API_BASE_URL + '/bounces/search?comments=1';
    var queryUrl = baseUrl + '&lat='+ urlParams.latitude +'&limit='+ urlParams.limit +'&long='+ 
      urlParams.longitude +'&offset=' + urlParams.offset + '&distance=' + urlParams.distance + '&eternity=1';

    // display by 0:recent or by 1:popularity
    if(mode === 1) {
      queryUrl += '&feed=popular';
    }

    // get the bounces with no constrains for the distance
    if(infinity === 1) {
      queryUrl += '&infinity=1';
    }

    if(Globals.VERBOSE >= 2) console.log(Globals.LOG_PREFIX + ' | queryUrl: ' + queryUrl);

    return $http.get(queryUrl);

	    // .success(function(data) {
	    // 	this.newItems = data;
	    // }

	    // .bind(this));
  };

  return new Bounce;

}]);