angular.module('BounceApp')

.controller('CreateBounceController', 
  ['$scope', '$http', '$q', 'Globals', 'localStorageService', 'hello', 'Upload',
  function($scope, $http, $q, Globals, localStorageService, hello, Upload) {

    var timeoutCount = 0;
    var timeoutCountReloadMax = 4;

    /**
     * Default state
     */
    $scope.bounce = getBounceDefaultValues();
    $scope.visibleBounceControls = false;
    $scope.fullHeightTextarea = false;

    /**
     * validImageFormats list all the valid image types
     */
    $scope.validImageFormats = {
      jpeg: true,
      jpg: true,
      png: true
    };

    /**
     * misc scope variables
     */
    $scope.textareaPlaceholder = 'Share photos, pics and events!';

    /**
     * errors
     */
    $scope.bounceTextOrPictureRequiredError = false;
    $scope.bounceExpirationRadiusRequiredError = false;
    $scope.bouncePictureFormatError = false;
    $scope.bouncePictureTooLargeError = false; // TODO: implement compression library and ask max accepted file size for the API 
    $scope.bouncePictureStillUploadingError = false; // TODO: implement compression library and ask max accepted file size for the API 

    /**
     * picture upload
     */
    $scope.pictureToUpload = null;
    $scope.pictureUploadedId = null;
    $scope.pictureUrl = null;

    /**
     * login variables
     */
    $scope.userName = getStoredUserName();
    $scope.userLoggedIn = isUserLoggedIn();

    var facebook = {
      name: 'facebook',
      loggedIn: isUserLoggedInWithFacebook(),
      idName: Globals.FB_ID,
      tokenName: Globals.FB_TOKEN 
    };

    var twitter = {
      name: 'twitter',
      loggedIn: isUserLoggedInWithTwitter(),
      idName: Globals.TWIT_ID,
      tokenName: Globals.TWIT_TOKEN
    };

    /**
     * isUserLoggedIn check if the user is logged in with Bounce API
     * @return boolean return true if the user is logged in
     */
    function isUserLoggedIn() {
      if(localStorageService.get(Globals.API_KEY) !== null) {
        return true;
      }

      return false;
    }

    /**
     * isUserLoggedInWithFacebook check if the user is logged in with Facebook API
     * @return boolean return true if the user is logged in
     */
    function isUserLoggedInWithFacebook() {
      if(localStorageService.get(Globals.FB_ID) !== null && localStorageService.get(Globals.FB_TOKEN) !== null) {
        return true;
      }

      return false;
    }

    /**
     * isUserLoggedInWithTwitter check if the user is logged in with Twitter API
     * @return boolean return true if the user is logged in
     */
    function isUserLoggedInWithTwitter() {
      if(localStorageService.get(Globals.TWIT_ID) !== null && localStorageService.get(Globals.TWIT_TOKEN) !== null) {
        return true;
      }

      return false;
    }

    /**
     * setStoredUserName get the user name from the local storage if it exists
     */
    function getStoredUserName() {
      var userName = localStorageService.get(Globals.USER_NAME);
      if(userName !== null) {
        return userName;
      } else {
        return '';
      }
    }

    /**
     * getBounceDefaults get the bounce object in its default state
     * @return object bounce object
     */
    function getBounceDefaultValues() {
      return {
        text: '',
        picture: null,
        radius: '-1',
        expiration: '-1',
        location: null,
        shareFacebook: null,
        shareTwitter: null
      };
    }

    /**
     * loginToBounceWithFacebook login with Facebook
     */
    $scope.loginToBounceWithFacebook = function() {
      loginWithSocialNetwork(facebook);
    }
    
    /**
     * loginToBounceWithTwitter login with twitter
     */
    $scope.loginToBounceWithTwitter = function() {
      loginWithSocialNetwork(twitter);
    }

    /**
     * loginWithSocialNetwork generic function for login with a social network
     * @param  object socialNetwork information about the social network
     */
    function loginWithSocialNetwork(socialNetwork) {
      if(Globals.VERBOSE >= 2) console.log(Globals.LOG_PREFIX + ' | loginWithSocialNetwork...');
      if(Globals.VERBOSE >= 2) console.log(Globals.LOG_PREFIX + ' | login with ' + socialNetwork.name + '...');

      hello(socialNetwork.name).login().then( 
        function(response) {
          var access_token = response.authResponse.access_token;
          socialNetwork.loggedIn = true;

          if(Globals.VERBOSE >= 3) console.log(Globals.LOG_PREFIX + ' | hello login response: ');
          if(Globals.VERBOSE >= 3) console.log(response);

          // get user information: id and name
          hello(socialNetwork.name).api('me').then(
            function(response) {
              if(Globals.VERBOSE >= 1) console.log(Globals.LOG_PREFIX + ' | logged in with ' + socialNetwork.name);

              $scope.userName = response.name;
              localStorageService.set(Globals.USER_NAME, response.name);
              localStorageService.set(socialNetwork.idName, response.id);
              localStorageService.set(socialNetwork.tokenName, access_token);

              loginWithBounceAPI({
                id: response.id,
                token: access_token
              }); 

            }, function(e) {
              console.error(Globals.ERROR_PREFIX + ' | with the error: ' + JSON.stringify(e, null, 1));
              if(Globals.VERBOSE >= 1) console.log(Globals.ERROR_PREFIX + ' | logout from ' + socialNetwork.name + '...');
              $scope.logoutFromSocialNetwork(socialNetwork);
            }
          );
        }, 
        function(e) {
          console.error(Globals.ERROR_PREFIX + ' | ' + socialNetwork.name + ' login error: ' + e.error.message);
        }
      );
    }

    /**
     * loginWithBounceAPI login with the bounce API
     * @param  object response the object with the client_id and access_token
     */
    function loginWithBounceAPI(auth) {
      if(isUserLoggedIn()) {
        return;
      }

      if(Globals.VERBOSE >= 3) console.log(JSON.stringify(auth, null, 1));

      // prepare the request object for the AJAX HTTP request
      var request = {
        method: 'POST',
        url: (Globals.API_BASE_URL + '/login'),
        data: {
          "fb_id": auth.id,
          "fb_token": auth.token
        }
      };

      $http(request).then(
        function(response) {
          if(Globals.VERBOSE >= 1) console.log(Globals.LOG_PREFIX + ' | logged in with bounce');
          if(Globals.VERBOSE >= 3) console.log(Globals.LOG_PREFIX + ' | Bounce API login response JSON: ' + JSON.stringify(response, null, 1));

          // store the token in localStorage object
          localStorageService.set(Globals.API_KEY, response.data.token);
          $scope.userLoggedIn = true;
          $scope.visibleBounceControls = false;

        }, 
        function(response) { // if error
          console.error(Globals.ERROR_PREFIX + ' | fail callback running!');
        }
      );
    }

    /**
     * loginForSharingWithFacebook be sure user is login with facebook if share with facebook checkbox checked
     */
    $scope.loginForSharingWithFacebook = function() {
      if($scope.bounce.shareFacebook && facebook.loggedIn === false) {
        $scope.loginToBounceWithFacebook();
      }
    };

    /**
     * loginForSharingWithTwitter be sure user is login with twitter if share with facebook checkbox checked
     */
    $scope.loginForSharingWithTwitter = function() {
      if($scope.bounce.shareTwitter && twitter.loggedIn === false) {
        $scope.loginToBounceWithTwitter();
      }
    };

    /**
     * logout from Bounce AND social network
     */
    $scope.logout = function() {
      if(Globals.VERBOSE >= 2) console.log(Globals.LOG_PREFIX + ' | logout');
      if(Globals.VERBOSE >= 2) console.log(Globals.LOG_PREFIX + ' | logout from bounce...');
      if(Globals.VERBOSE >= 2 && facebook.loggedIn) console.log(Globals.LOG_PREFIX + ' | logout from facebook...');
      if(Globals.VERBOSE >= 2 && twitter.loggedIn) console.log(Globals.LOG_PREFIX + ' | logout from twitter...');

      var logoutPromises = [];

      if(facebook.loggedIn) {
        logoutPromises.push(logoutFromSocialNetwork(facebook));
      }

      if(twitter.loggedIn) {
        logoutPromises.push(logoutFromSocialNetwork(twitter));
      }

      // when all the social networks logouts are done $q.all promise is returned
      $q.all(logoutPromises).then(
        function(response) {
          if(localStorageService.get(Globals.API_KEY) !== null) {
            localStorageService.remove(Globals.API_KEY); 
            if(Globals.VERBOSE >= 1) console.log(Globals.LOG_PREFIX + ' | logged out from bounce'); 
          }

          if(localStorageService.get(Globals.USER_NAME) !== null) {
            localStorageService.remove(Globals.USER_NAME);  
          }

          $scope.resetDefaults();
        },
        function(e) {
          console.error(Globals.ERROR_PREFIX + ' | ' + socialNetwork.name + ' logout error: ' + e.error.message);
          $scope.userLoggedIn = true;
        }
      );

      // UX: fast logout impression 
      $scope.userLoggedIn = false;
    }

    /**
     * logoutFromSocialNetwork generic function for logout from the API(s)
     * @param  object socialNetwork the information about the social network
     * @return object promise       when the logout is done the promise will call the callback functions
     */
    function logoutFromSocialNetwork(socialNetwork) {
      var deferred = $q.defer();

      if(Globals.VERBOSE >= 2) console.log(Globals.LOG_PREFIX + ' | logoutFromSocialNetwork');

      hello(socialNetwork.name).logout().then(
        function(response) {
          if(Globals.VERBOSE >= 1) console.log(Globals.LOG_PREFIX + ' | logged out from ' + socialNetwork.name);
          socialNetwork.loggedIn = false;

          if(localStorageService.get(socialNetwork.idName) !== null) {
            localStorageService.remove(socialNetwork.idName);
          }

          if(localStorageService.get(socialNetwork.tokenName) !== null) {
            localStorageService.remove(socialNetwork.tokenName);
          }

          deferred.resolve(response);
        }, 
        function(e) {
          if(Globals.VERBOSE >= 2) console.log(Globals.ERROR_PREFIX + ' | ' + socialNetwork.name + ' logout: ' + e.error.message);
          if(Globals.VERBOSE >= 3) console.log(e);
          clearUserLoginData(socialNetwork);
          deferred.resolve(e);
        }
      );

      return deferred.promise;
    }

    /**
     * clearUserLoginData clear the local storage from the social network login credentials 
     * and reset the boolean to false
     */
    function clearUserLoginData(socialNetwork) {
      socialNetwork.loggedIn = false;

      if(localStorageService.get(socialNetwork.idName) !== null) {
        localStorageService.remove(socialNetwork.idName);
      }

      if(localStorageService.get(socialNetwork.tokenName) !== null) {
        localStorageService.remove(socialNetwork.tokenName);
      }
    }

    /**
     * resetDefaults reset the bounce application to the defaults
     */
    $scope.resetDefaults = function() {
      if(Globals.VERBOSE >= 2) console.log(Globals.LOG_PREFIX + ' | resetDefaults...');
      if(Globals.VERBOSE >= 3) console.log(Globals.LOG_PREFIX + ' | resetDefaults | bounce object keys: ' + Object.keys($scope.bounce));

      if($scope.bounce.flow && $scope.bounce.flow.files.length !== 0) {
        $scope.bounce.flow.cancel();
      }

      $scope.fullHeightTextarea = false;
      $scope.visibleBounceControls = false;
      $scope.bounce = getBounceDefaultValues();
    };

    /**
     * createBounce create a bounce and send it to the Bounce API
     */
    $scope.createBounce = function() {
      if($scope.pictureUploadedId === null) {
        // $scope.bouncePictureStillUploadingError = true;
        $scope.bouncePictureTooLargeError = true;
        return;
      } else {
        // $scope.bouncePictureStillUploadingError = false;
        $scope.bouncePictureTooLargeError = false;
      }

      if(areValidationRequirementsMet() === false) {
        $scope.visibleBounceControls = true;
        return;
      }

      if ($scope.bounce.text === '') {
        $scope.bounce.text = $scope.textareaPlaceholder;
      }

      var pictureArray = $scope.pictureUploadedId !== null ? [$scope.pictureUploadedId] : [];

      var bounceToSend = {
        "message": $scope.bounce.text,
        "latitude": Globals.LATITUDE,
        "longitude": Globals.LONGITUDE,
        "category": 1,
        "length": $scope.bounce.expiration.toString(),
        "files": pictureArray, 
        "scope": parseFloat($scope.bounce.radius),
        "locationVisibility": $scope.bounce.location ? "1" : "0",
        "infinity": $scope.bounce.radius == "infin" ? 1 : 0,
        "eternity": $scope.bounce.expiration == "eternity" ? 1 : 0
      };

      if(Globals.VERBOSE >= 3) console.log(Globals.LOG_PREFIX + ' | bounce to send: ' + JSON.stringify(bounceToSend, null, 1));

      $http({
        method: 'POST',
        url: Globals.API_BASE_URL + '/bounces/create',
        data: bounceToSend,
        headers: {
          'Authorization': ('Bearer ' + localStorageService.get(Globals.API_KEY)),
          'Content-Type': 'application/json'
        }
      }).then(
        function(response) {

          if(Globals.VERBOSE >= 1) console.log(Globals.LOG_PREFIX + ' | Bounce has been created');
          if(Globals.VERBOSE >= 2) console.log(Globals.LOG_PREFIX + ' | Bounce response object : ' + JSON.stringify(response, null, 1));

          var url = response.data.files[0].url;
          $scope.pictureUrl = {
            facebook: url,
            twitter: url.substring(7)
          };

          if($scope.bounce.shareFacebook) {
            shareWithSocialNetwork('facebook');
          }

          if($scope.bounce.shareTwitter) {
            shareWithSocialNetwork('twitter');
          }

          $scope.resetDefaults();

        }, function(e) {
          console.error(Globals.ERROR_PREFIX + ' | couldn\'t create the Bounce with the error: ' + JSON.stringify(e, null, 1));
        }
      );
    };

    /**
     * areAllRequirementsMet bounce creation validation step
     * @return boolean return true if the validation requirements are met 
     */
    function areValidationRequirementsMet() {
      if ($scope.bounce.text === '' && $scope.pictureUploadedId === null) {
        $scope.bounceTextOrPictureRequiredError = true;
        return false;
      }

      if($scope.bounce.expiration == -1 || $scope.bounce.radius == -1) {
        $scope.bounceExpirationRadiusRequiredError = true;
        return false;
      } 
      
      $scope.bounceTextOrPictureRequiredError = false;
      $scope.bounceExpirationRadiusRequiredError = false;

      return true;
    }

    /**
     * shareWithSocialNetwork share with a social network
     * @param  string name the name of the social network with which we want to share
     */
    function shareWithSocialNetwork(name) {
      if(Globals.VERBOSE >= 2) console.log(Globals.LOG_PREFIX + ' | picture link: ' + $scope.tempPictureUrl);

      var data = {
        message: $scope.bounce.text
      };

      if($scope.pictureUrl) {
        data.picture = (name === 'facebook') ? $scope.pictureUrl.facebook : $scope.pictureUrl.twitter;
      }

      hello(name).api('me/share', 'post', data).then(
        function(reponse) {
          if(Globals.VERBOSE >= 1) console.log(Globals.LOG_PREFIX + ' | Bounce has been shared with ' + name);
        }, function(e) {
          console.error(Globals.ERROR_PREFIX + ' | with the error: ' + JSON.stringify(e, null, 1));
        }
      );
    }

    /**
     * description file added event
     * @param  object event     the file::added event object
     * @param  object $flow     the ng-flow object
     * @param  object flowFile  the flow file object
     */
    $scope.$on('flow::fileAdded', function (event, $flow, flowFile) {
      if(Globals.VERBOSE >= 2) console.log(Globals.LOG_PREFIX + ' | flow::fileAdded event triggered');
      if(Globals.VERBOSE >= 2) console.log(Globals.LOG_PREFIX + ' | $flow: ' + $flow);

      $scope.bounce.flow = $flow;
      $scope.pictureToUpload = flowFile.file;

      uploadFile(flowFile.file);
    });

    /**
     * uploadFile upload the specified file to the Bounce API
     * @param  object   file the file to upload
     */
    function uploadFile(file) {
      Upload.upload({
        url: Globals.API_BASE_URL + '/bounces/picture',
        file: { 
          picture: file 
        },
        method: 'POST',
        headers: {
          'Authorization': 'Bearer ' + localStorageService.get(Globals.API_KEY),
          'Content-Type': 'multipart/form-data'
        }, 
        fileName: file.name,
        fileFormDataName: 'picture',
      })
      .progress(function (evt) {
        $scope.progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
        if(Globals.VERBOSE >= 3) console.log(Globals.LOG_PREFIX + ' | file uploaded progress: ' + $scope.progressPercentage + '% ' + evt.config.file.name)
      })
      .success(function(response){
        if(Globals.VERBOSE >= 1) console.log(Globals.LOG_PREFIX + ' | file uploaded id: ' + response.fileId);
        $scope.pictureUploadedId = response.fileId;
        timeoutCount = 0;
      })
      .error(function(e){
        console.log(e);
      });
    }

}]); // end of CreateBounceController


// 