
angular.module('BounceApp')

	.controller('CommentsController', ['$scope', 'bounces', function($scope, bounces) {

		// set the bounce limit
		var LIMIT = 2;

		$scope.init = function(bounce) {
			// save the bounce for later
			$scope.bounceCopy = bounce;

			// limit the bounce array to only 2 items
			$scope.comments = bounce.hasOwnProperty('comments') ? bounce.comments.slice(0, LIMIT) : [];
		};

		$scope.loadAllComments = function() {
			$scope.comments = $scope.bounceCopy.comments;
			$scope.hideButton = true;
		};
	}]);