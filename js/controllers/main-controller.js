
angular.module('BounceApp')

.controller('MainController', 
  ['$scope', '$interval', '$timeout', '$http', 'bounces', 'Globals',
  function($scope, $interval, $timeout, $http, bounces, Globals) {
  
  // CONSTANTS
  // limit which is going to be triggered the first time: allow a fast loading
  var INITIAL_LIMIT = 20,

  // set the number of time the "bottom-reached" event needs to be triggered to call the REST API
  BOTTOM_REACHED_QUERY_FREQ = 1,

  // No way to bind it properly to the directive's attribute
  PAGE_SIZE = 10,

  // limit and offset are dependant of the 
  LIMIT = BOTTOM_REACHED_QUERY_FREQ * PAGE_SIZE * 2,

  // set the offset for the additional API calls
  OFFSET_INC = 0,

  // interval values for the refresh functionality
  INTERVAL = 5000,
  INTERVAL_SCROLL_TOP = 600,

  // distances that map the input slider values
  // the chosen distance will set the distance and position the slider
  DISTANCES = [91.44, 1609.34, 16093.4, 160934, -1],
  CHOSEN_DISTANCE = 5, // {[1-5]+}

  TIMEOUT_RELOAD_MAX = 4;

  // VARIABLES
  var timeoutCount = 0;

  // the interval used for the refresh
  var currentInterval;

  var bottomReachedCounter = 0;

  // resetted when BOTTOM_REACHED_QUERY_FREQ is reached
  var bottomReachedFreqCounter = 0;

  var firstQuery = true;


  // the bounces array
  $scope.bounces = [];

  // default radio button
  $scope.radioModel = 'Everyone';

  // default mode: sorted by recent posts
  $scope.mode = 0;

  // geolocation messages
  $scope.geoLocationMessage = true;
  $scope.geoLocationError = false;

  /**
   * Defaults Parameters for the GET resquest to the Bounce API
   *
   * @prop int distance the chosen distance
   * @prop string latitude the latitude of the user's computer
   * @prop string longitude the longitude of the user's computer
   * @prop int limit limit the number of bounces showed
   */
  var urlParams = {
    distance: DISTANCES[CHOSEN_DISTANCE - 1],
    latitude: null,
    longitude: null,
    limit: LIMIT, 
    offset: 0
  }

  // values for the slider
  $scope.slider = {
    value: CHOSEN_DISTANCE - 1,
    oldValue: 0
  };

  /**
   * get the geolocation from the browser and launch the init function
   */
  if(navigator.geolocation) {
    // 4000 is the minimum according to our tests to not have a timeout error with gotErr
    var timeout = navigator.userAgent.indexOf("android") > -1 ? '10000' : '5000';

    // Trap a GPS error, log it to console and display on site
    function gotErr(error) {
      var errors = {
              1: 'Permission denied',
              2: 'Position unavailable',
              3: 'Request timeout'
          };
      console.error(Globals.ERROR_PREFIX + ' | with the error: ' + errors[error.code]);
      if(Globals.VERBOSE >= 1) console.log(Globals.LOG_PREFIX + ' | reload document...');
      
      if(timeoutCount <= TIMEOUT_RELOAD_MAX) {
        $timeout(function() {
          timeoutCount++;
          location.reload();
        }, 1000);
      }
    }

    var options = {
      enableHighAccuracy: true,
      timeout: timeout,
      maximumAge: 1000
    };

    // getCurrentPosition() and watchPosition() are deprecated on insecure origins, 
    // and support will be removed in the future. 
    // You should consider switching your application to a secure origin, such as HTTPS. 
    // See https://goo.gl/rStTGz for more details.
    var watchID = navigator.geolocation.watchPosition(init, gotErr, options);
    var timeoutResult = setTimeout(function() { navigator.geolocation.clearWatch(watchID); }, timeout);

    // navigator.geolocation.getCurrentPosition(init, gotErr, options);
  } else {
    console.error(Globals.ERROR_PREFIX + ' | Geolocation is not available with your browser.');
    $scope.geoLocationError = true;
  }

  /**
   * init set the location and latitude and list the bounces for the first time
   *
   * @var object position the geolocation position object
   */
  function init(position) {
    // hide the Geo Location message since we got the geo information
    $scope.geoLocationMessage = false;
    $scope.$apply(); // force the digest loop to hide the get location message

    Globals.LATITUDE = urlParams.latitude = position.coords.latitude;
    Globals.LONGITUDE = urlParams.longitude = position.coords.longitude;  

    // once we have a location, let's get the bounces for the first time
    queryBounceAPI();
  }

  /**
   * slideEnded this function is called each time the user release the slider on some value
   * because of the databinding of angular, to avoid multiple calls we store the value
   * to compare it with the next one for each call
   */
  $scope.slideEnded = function() {
    if($scope.slider.value !== $scope.slider.oldValue) {

      $scope.slider.oldValue = $scope.slider.value;
      urlParams.distance = DISTANCES[$scope.slider.value];

      resetView();

      // get the bounces, reset and fill the $scope.bounces array
      queryBounceAPI(true);
    }
  }

  /**
   * changeDisplayMode click on one of the "Recent" or "Popular" buttons
   * @var int mode 0 for Recent and 1 for Popular
   */
  $scope.changeDisplayMode = function(mode) {
    $scope.mode = mode;

    resetView();

      // get the bounces, reset and fill the $scope.bounces array
    queryBounceAPI(true);
  };

  /**
   * resetBouncesView reset the view variables
   */
  function resetView() {
    firstQuery = true;
    urlParams.offset = 0;
    bottomReachedCounter = 0;
    bottomReachedFreqCounter = 0;
  }

  /**
   * stopCurrentInterval stop the current interval
   */
  function stopCurrentInterval() {
    if (angular.isDefined(currentInterval)) {
      $interval.cancel(currentInterval);
      currentInterval = undefined;
    }
  }

  /**
   * refreshBouncesAtInterval get n more bounces every x milliseconds 
   */
  function refreshBouncesAtInterval() {
    if((document.getElementById('top').scrollTop) <= INTERVAL_SCROLL_TOP) {
      resetView();

      // get the bounces, reset and fill the $scope.bounces array
      // restartRefresh = false
      queryBounceAPI(true, false);
    }
  }

  /**
   * Add more bounces to the $scope.bounces array every n time 
   * the bottom-reached event is triggered
   */
  $scope.$on('bottom-reached', function() {
    bottomReachedCounter++;
    bottomReachedFreqCounter++;
    
    if(bottomReachedFreqCounter === BOTTOM_REACHED_QUERY_FREQ || bottomReachedCounter < 3) {
      bottomReachedFreqCounter = 0;
      // the first time the "bottom-reached" event is triggered 
      // we need to increment exceptionally the offset by the initial limit
      if(bottomReachedCounter > 1) {
        urlParams.offset += LIMIT;
      } else {
        urlParams.offset = 0;
      }
      
      // get the bounces and update the $scope.bounces array (no reset)
      queryBounceAPI(false);
    }
  });


  /**
   * queryBounceAPI update the $scope.bounces array
   *
   * @param bool reset reset the $scope.bounces array and other variables
   * @param bool refresh the parameters to customize the URL
   */
  function queryBounceAPI(reset, restartRefresh) {  
    // default value for restartRefresh
    restartRefresh = typeof restartRefresh !== 'undefined' ? restartRefresh : true;

    if(urlParams.latitude === null || urlParams.longitude === null) {
      return;
    }

    // keep the urlParams defaults, just pass a deep copy
    var params = angular.copy(urlParams);

    if(firstQuery === true) {
      // update the limit to the one related to BOTTOM_REACHED_QUERY_FREQ
      params.limit = INITIAL_LIMIT;
    }

    bounces.nextPage($scope.mode, params)
      .success(function(newBounces) {
        // if $scope.bounces array is different from newBounces array
        if(firstQuery === true 
          || $scope.bounces.length != newBounces.length
          || compareBouncesData($scope.bounces, newBounces) === false
        ) {
          if(reset === true) {
            // reset $scope.bounces array and other critical variables
            $scope.bounces = [];
          }
          // cashing the length in l for performances
          for (var i = 0, l = newBounces.length; i < l; i++) {
            $scope.bounces.push(newBounces[i]);
          }
        }
        
        if(firstQuery === true && restartRefresh === true) {
          // stop the interval before starting a new one
          stopCurrentInterval();

          // restart the refresh interval
          currentInterval = $interval(refreshBouncesAtInterval, INTERVAL);
        }

        // the API has been called for the "first" time
        firstQuery = false;

      });
  }

  /**
   * compareBouncesData linear comparison between two arrays 
   * based on the bounces ID AND strict about the order
   * 
   * @param  array bouncesArr1 the first array of bounces to compare
   * @param  array bouncesArr2 the second array of bounces to compare
   * @return bool true if the two arrays are the same ones
   */
  function compareBouncesData(bouncesArr1, bouncesArr2) {
    for (var i = 0, l = $scope.bounces.length; i < l; i++) {
      if(bouncesArr1[i].bounceId !== bouncesArr2[i].bounceId) {
        return false;
      }
    }

    // since all the bounce IDs were the same we return true
    return true;
  }

}]); // end MainController
