
angular.module('BounceApp', 
	[
		'ngRoute', 
		'ngSanitize', 
		'bouncefilters', 
		'ui.bootstrap', 
		'ui.bootstrap-slider', 
		'ks.ngScrollRepeat',
    'LocalStorageModule',
		'flow',
    'ngHello',
    'ngFileUpload'
	]
)

.config(['helloProvider', function(helloProvider) {
  helloProvider.init({
    twitter: 'zjGzJv76ce6PB4Ey2eZKp71a0',
    facebook: '373372816160699'
  }, {
    redirect_uri: 'include/redirect.html'
  });
}])

.config(['localStorageServiceProvider', function (localStorageServiceProvider) {
  localStorageServiceProvider
    .setPrefix('ls')
    .setStorageType('localStorage');
}])

.config(['$httpProvider', 'flowFactoryProvider', 
  function($httpProvider, flowFactoryProvider) {
    $httpProvider.defaults.useXDomain = true;
  	delete $httpProvider.defaults.headers.common["X-Requested-With"];
  	$httpProvider.defaults.headers.common["Accept"] = "application/json";
  	$httpProvider.defaults.headers.common["Content-Type"] = "application/json"; 

  	flowFactoryProvider.defaults = {
      target: '',
      permanentErrors: [500, 501],
      maxChunkRetries: 1,
      chunkRetryInterval: 5000,
      simultaneousUploads: 1
    };

}])

.factory('Globals', function() {
  return {
    API_BASE_URL: 'http://ibounce.co:8678',

    API_KEY: 'api_key',
    FB_ID: 'fb_id',
    FB_TOKEN: 'fb_token',
    TWIT_ID: 'twitter_id',
    TWIT_TOKEN: 'twitter_token',
    USER_NAME: 'username',

    // VERBOSE
    // 0: none
    // 1: basic
    // 2: advanced
    // 3: all
    VERBOSE: 1,

    ERROR_PREFIX: 'BOUNCEAPP_ERROR',
    LOG_PREFIX: 'BOUNCEAPP',

    LATITUDE: null,
    LONGITUDE: null,

    // refresh when scroll top is collapsed or not
    SCROLL_TOP_COLLAPSED: 400,
    SCROLL_TOP_EXPANDED: 600,
    refreshWhenScrollTop: this.SCROLL_TOP_COLLAPSED
  };
});

