
angular.module('BounceApp')

.directive('dynamicPlaceholder', function() {
  return {                
    restrict: 'A',
    link: function ($scope, element, attrs) {
      attrs.$observe('dynamicPlaceholder', function(value) {
        element.attr('placeholder', value);
      });
    }
  };
});