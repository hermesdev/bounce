
angular.module('BounceApp')

.directive('uploadfile', function () {
  return {
    restrict: 'A',
    link: function(scope, element) {

      element.bind('click', function(e) {
        document.getElementById('file-upload').click();
      });
    }
  };
});