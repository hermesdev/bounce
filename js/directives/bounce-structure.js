
angular.module('BounceApp').directive('bounceStructure', function() {
	return {
		restrict: 'E',
		scope: {
			bounce: '='
		},
		templateUrl: 'js/directives/bounce-structure.html'
	};
});
