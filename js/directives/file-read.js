angular.module('BounceApp')

.directive("fileread", [function () {
  return {
    scope: {
      fileread: "="
    },
    link: function (scope, element, attributes) {
      element.bind("change", function (changeEvent) {
        scope.$apply(function () {
          // only one selected file
          console.log(changeEvent.target.files[0]);
          scope.fileread = changeEvent.target.files[0];

          // or all selected files:
          // scope.fileread = changeEvent.target.files;
        });
      });
    }
  }
}]);