$(document).ready(function() {

    // http://stackoverflow.com/questions/2400935/browser-detection-in-javascript
    var ua = navigator.userAgent.toLowerCase();
    var check = function(r) {
        return r.test(ua);
    };

    var DOC = document;
    var isChrome = check(/chrome/);
    var isWebKit = check(/webkit/);
    var isiPad = check(/iPad/i);
    var isiOS = check(/iPad|iphone|ipod/i);
    var isiphone = check(/iphone/i);
    var chromeIos = check(/CriOS/i);


    if(isiphone && isChrome || chromeIos) {
        $('html').addClass('iphone-chrome');
    } 

});