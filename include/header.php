<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>BounceChat</title>

	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />

	<!-- fonts -->
	<link rel='stylesheet' href='https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css'>
	<link rel='stylesheet' href='http://fonts.googleapis.com/css?family=Roboto'>
	<!-- Bootstrap CSS-->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css" />
	<link rel="stylesheet" href="css/bootstrap-social.css">
	<link rel="stylesheet" href="css/bootstrap-slider.css">
	<!-- swipebox -->
	<link rel="stylesheet" href="css/swipebox.min.css">
	<!-- custom CSS -->
	<link rel="stylesheet" href="css/style.css">

	<!-- html5 compatibility -->
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.2/html5shiv.min.js"></script>

	<!-- lib js files -->
	<script src="https://code.jquery.com/jquery-2.1.4.min.js"></script>
	<script src="js/lib/jquery.oauthpopup.js"></script>

	<!-- AngularJS modules -->
  <!-- <script src="//ajax.googleapis.com/ajax/libs/angularjs/1.3.5/angular.min.js"></script> -->
  <!-- <script src="//code.angularjs.org/1.2.28/angular-route.min.js"></script> -->
	<script src="bower_components/angular/angular.js"></script>
	<script src="bower_components/angular-route/angular-route.js"></script>
  
	<script src="js/lib/angular-sanitize.min.js"></script>

	<script src="js/lib/ui-bootstrap-tpls-0.13.0.min.js"></script>
	<script src="js/lib/bootstrap-slider.js"></script>
	<script src="js/lib/ui-bootstrap-slider.js"></script>
	<script src="js/lib/ng-scroll-repeat.js"></script>
	<!-- swipebox -->
	<script src="js/lib/jquery.swipebox.min.js"></script>
	<script src="js/lib/ng-flow-standalone.min.js"></script>

	<script src="bower_components/angular-local-storage/dist/angular-local-storage.min.js"></script>
	<script src="bower_components/angular-facebook/lib/angular-facebook.js"></script>
	<script src="bower_components/hello/dist/hello.all.js"></script>
	<script src="bower_components/ng-hello/dist/ng-hello.js"></script>
	<script src="bower_components/ng-file-upload/ng-file-upload.min.js"></script>
	<script src="bower_components/ng-file-upload/ng-file-upload-shim.min.js"></script>

	<!-- cross browser -->
	<!-- <script src="js/cross-browser.js"></script> -->

	<!-- !!!HARD RELOAD FOR DEV ONLY!!! -->
	<meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate"/>
	<meta http-equiv="Pragma" content="no-cache"/>
	<meta http-equiv="Expires" content="0"/>
	<!-- !!!HARD RELOAD FOR DEV ONLY!!! -->
</head>

<body ng-app="BounceApp" ng-controller="MainController" id="top">

	<div id="geoloc-waiting" ng-show="geoLocationMessage">
		<h2>Waiting for the browser to get location information...</h2>
	</div>

	<div id="geoloc-error" ng-show="geoLocationError">
		<h2>Sorry, your device geolocation is not functional!</h2>
	</div>

	<div ng-controller="CreateBounceController">
		<header>
			<div class="login-bar clearfix">
				<div class="layout-wrapper">
					<ul>
						<li>
							<a class="btn btn-social btn-facebook" ng-click="loginToBounceWithFacebook()" ng-hide="userLoggedIn">
	  						<i class="fa fa-facebook"></i>
	  						Login with Facebook
							</a>
							<!-- <a class="btn btn-social btn-twitter" ng-click="loginToBounceWithTwitter()" ng-hide="userLoggedIn">
	  						<i class="fa fa-twitter"></i>
	  						Login with Twitter
							</a> -->
							<span ng-show="userLoggedIn">Logged in as {{ userName }}</span>
							<a class="logout" ng-click="logout()" ng-show="userLoggedIn">Logout</a>
						</li>
					</ul>
				</div>
			</div>

			<h1>
				<img class="visible-xs img-responsive header-image" src="img/bouncechat.png" alt="BounceChat"/>
				<img class="hidden-xs img-responsive header-image" src="img/bouncechat.png" alt="BounceChat"/>
				<div class="header-links">
					<a href="https://geo.itunes.apple.com/us/app/bouncechat-share-moments-nearby!/id949059449?mt=8"><img class="img-responsive apple-link" src="img/badge_appstore-lrg.svg" alt="Apple Store Link"/></a>
					<a href="http://info.bouncechat.com" class="btn btn-default info-link">More Info</a>
				</div>
			</h1>
		</header>