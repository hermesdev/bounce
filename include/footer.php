<!-- fire the swipebox plugin -->
	<script type="text/javascript">
		;( function( $ ) {

			$('.swipebox').swipebox();

		} )( jQuery );
	</script>

	<!-- project js files -->
	<!-- Modules -->
	<script src="js/app.js"></script>
	<!-- Controllers -->
	<script src="js/controllers/main-controller.js"></script>
	<script src="js/controllers/comments-controller.js"></script>
	<script src="js/controllers/create-bounce-controller.js"></script>
	<!-- Services -->
	<script src="js/services/bounces.js"></script>
	<!-- Directives -->
	<script src="js/directives/bounce-structure.js"></script>
	<script src="js/directives/upload-file.js"></script>
	<script src="js/directives/file-read.js"></script>
	<script src="js/directives/valid-file.js"></script>
	<script src="js/directives/dynamic-placeholder.js"></script>
	<!-- filters -->
	<script src="js/filters/functions.js"></script>
	<script src="js/filters/module.js"></script>
	<script src="js/filters/pretty-date.js"></script>
	<script src="js/filters/to-fixed.js"></script>
	<script src="js/filters/to-trust.js"></script>

</body>
</html>