# BounceChat

Social network based on the geolocalization of the users to share events and more. 

## Dependencies

Install the dependencies with the following commands:
```
npm install will install both "dependencies" and "devDependencies"

npm install --production will only install "dependencies"

npm install --dev will only install "devDependencies"
```